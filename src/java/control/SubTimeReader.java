
package control;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * Manages whether the submission is allowed on the index page
 */

@ManagedBean
@RequestScoped
public class SubTimeReader {
    
    private boolean tabViewrendered;    // the submission is allowed, the tabview can be rendered
    private boolean subTimeRendered;    // the submission is forbidden , the text can be rendered
    private String subTime;     // the displayed text, outside submission time

    public SubTimeReader() {
        
        // opening the properties file
        Properties properties = new Properties();
        try {
            properties.load (new FileInputStream ("prop.properties"));
            }catch (IOException e) {
                e.printStackTrace();
            }
        
        // reading and creating the starting, ending and actual date
        int fromYear = Integer.parseInt(properties.getProperty("fromYear", String.valueOf(new Date().getYear())));
        int fromMonth = Integer.parseInt(properties.getProperty("fromMonth", String.valueOf(new Date().getMonth())));
        int fromDay = Integer.parseInt(properties.getProperty("fromDay", String.valueOf(new Date().getDate())));
        
        Date fromDate = new Date(fromYear, fromMonth, fromDay); 
        
        int endYear = Integer.parseInt(properties.getProperty("endYear", String.valueOf(new Date().getYear())));
        int endMonth = Integer.parseInt(properties.getProperty("endMonth", String.valueOf(new Date().getMonth())));
        int endDay = Integer.parseInt(properties.getProperty("endDay", String.valueOf(new Date().getDate())));
        
        Date endDate = new Date(endYear, endMonth, endDay);
        
        Date today = new Date();
        
        // comparing the dates
        if (today.after(fromDate) && today.before(endDate)) {
            tabViewrendered = true;
            subTimeRendered = false;
        } else{
            tabViewrendered = false;
            subTimeRendered = true;
        }
        
        // creating the text to show
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	subTime = new String("The submission is between " + dateFormat.format(fromDate) + " and " + dateFormat.format(endDate) + ".");
        
    }
    
    // Getters and Setters
    
    public boolean isTabViewrendered() {
        return tabViewrendered;
    }

    public void setTabViewrendered(boolean tabViewrendered) {
        this.tabViewrendered = tabViewrendered;
    }

    public boolean isSubTimeRendered() {
        return subTimeRendered;
    }

    public void setSubTimeRendered(boolean subTimeRendered) {
        this.subTimeRendered = subTimeRendered;
    }

    public String getSubTime() {
        return subTime;
    }

    public void setSubTime(String subTime) {
        this.subTime = subTime;
    }
    
    
}
