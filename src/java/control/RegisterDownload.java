/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import comp.Config;
import java.io.File;
import java.io.FileInputStream;
import javax.activation.MimetypesFileTypeMap;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 * For downloading the register csv file
 */

@ManagedBean
@RequestScoped
public class RegisterDownload {
    
    private StreamedContent file;

    public RegisterDownload() {
        try {
            
            File f = new File(Config.regDirectory + Config.regFileName);
            FileInputStream fis = new FileInputStream(f);
            file = new DefaultStreamedContent(fis, new MimetypesFileTypeMap().getContentType(f), Config.regFileName);
            
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        
        System.out.println("File downloaded: " + file.getName());
        
    }

    // Getter
    
    public StreamedContent getFile() {
        return file;
    }
    
}
