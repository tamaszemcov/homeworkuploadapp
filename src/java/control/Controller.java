
package control;


import entity.Task;
import utils.Comparison;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Controls the functionalities of the index page
 */

@ManagedBean(name="controller")
@SessionScoped ()
public class Controller {

    private boolean uploadDisabled; // indicates that the upload function is disabled
    
    private Task taskForm;  // results from the page    
    private Task taskDB;    // results from the data base
    
    private ResultSign resultSign;  
    private Student student;
    
    public static int studentTaskno;    //shared task number
    
    private int activeIndex = 0;    // the tab that must be shown
    
    public Controller() {
        this.taskForm = new Task();
        uploadDisabled = true;
        this.resultSign = new ResultSign();
        this.student = new Student();
       
    }

    /**
    * Compares the students results with the right ones
    */
    public String save() throws Exception{
       taskDB = readData();
       Boolean accepted = Comparison.compareTasks(taskForm, taskDB);
       System.out.println("Task results from form: ");
       System.out.println("Pn = " + taskForm.getPn());
       System.out.println("Puj = " + taskForm.getPuj());
       System.out.println("Prz = " + taskForm.getPrz());
       
       System.out.println("Task results from DB: ");
       System.out.println("Pn = " + taskDB.getPn());
       System.out.println("Puj = " + taskDB.getPuj());
       System.out.println("Prz = " + taskDB.getPrz());
        
       System.out.println("Task results are acceptable: " + accepted);
       
       if (Comparison.compareNumbers(taskForm.getPn(), taskDB.getPn())) resultSign.setPnSign("check_mark.png");
       else resultSign.setPnSign("x_mark.png");
       if (Comparison.compareNumbers(taskForm.getPrz(), taskDB.getPrz())) resultSign.setPrzSign("check_mark.png");
       else resultSign.setPrzSign("x_mark.png");
       if (Comparison.compareNumbers(taskForm.getPuj(), taskDB.getPuj())) resultSign.setPujSign("check_mark.png");
       else resultSign.setPujSign("x_mark.png");
       
       
       accepted=true; //csak a teszteléshez
       if (accepted)
       {
           setUploadDisabled(false);
           return "index?faces-redirect=true";
       }
       
       return null;
    }
    
    /**
    * Gets the data from the database
    */
    public Task readData(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("HFApp_DBPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        taskDB = em.find(Task.class, student.getTaskno());
        em.close();
        emf.close();
        return taskDB;
   }
    
    
    public Task getTaskForm() {
        return taskForm;
    }

    public void setTaskForm(Task taskForm) {
        this.taskForm = taskForm;
    }
    
    public void shareTaskno(){
        studentTaskno = student.getTaskno();
    }
    
    /**
    * changes the active tab
    */
    public String changeTab(int tab)
    {
        if (tab != 3)
        {
        activeIndex = tab;
        return "index?faces-redirect=true";
        }
        else
        {
            if (uploadDisabled) 
            {
                return null;
            }
            else
            {
                activeIndex = tab;
                return "index?faces-redirect=true";
            }
        }
    }
    
    // Getters and setters

    public boolean isUploadDisabled() {
        return uploadDisabled;
    }

    public void setUploadDisabled(boolean uploadDisabled) {
        this.uploadDisabled = uploadDisabled;
    }

    public ResultSign getResultSign() {
        return resultSign;
    }

    public void setResultSign(ResultSign resultSign) {
        this.resultSign = resultSign;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public int getActiveIndex() {
        return activeIndex;
    }

    public void setActiveIndex(int activeIndex) {
        this.activeIndex = activeIndex;
    }
    
}

