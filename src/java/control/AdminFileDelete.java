/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import comp.Config;
import java.io.File;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 * For deleting a given file in the list
 */

@ManagedBean
@RequestScoped
public class AdminFileDelete {
    
    public void delete()
    {
        Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String name = (String)params.get("name");
        System.out.println(name + " deleted");
        
        File fileToDel = new File(Config.directory+ name);
        fileToDel.delete();
        
    }
    
}
