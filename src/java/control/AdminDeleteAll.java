/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import comp.Config;
import java.io.File;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * For deleting all the submission files
 */

@ManagedBean
@RequestScoped
public class AdminDeleteAll {
    
    public void deleteFiles()
    {
        File directory = new File(Config.directory);
        File[] files = directory.listFiles();
        
        for (File file : files) {
            
            file.delete();
            
        }
    }
        
}
    

