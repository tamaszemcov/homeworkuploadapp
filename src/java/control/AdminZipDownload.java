/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import comp.Config;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.activation.MimetypesFileTypeMap;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 * For creating a zip from the filelist and downloading it
 */

@ManagedBean
@RequestScoped
public class AdminZipDownload {

    private StreamedContent zipFile;
    
    public AdminZipDownload() {
        
        // getting the list of files
        File directory = new File(Config.directory);
        File[] files = directory.listFiles();
        
        // creating the zip archive
        try {
            
            FileOutputStream fos = new FileOutputStream(Config.regDirectory + Config.zipFileName);
            ZipOutputStream zos = new ZipOutputStream(fos);
            
            for (File file : files) {
                
                System.out.println("Writing '" + file.getName() + "' to zip file");

		FileInputStream fis = new FileInputStream(file);
		ZipEntry zipEntry = new ZipEntry(file.getName());
		zos.putNextEntry(zipEntry);

		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zos.write(bytes, 0, length);
		}

		zos.closeEntry();
		fis.close();
                
            }
            
            zos.close();
            fos.close();
            
            
        } catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
        
        // downloading the zip file
        try {
            
            File f = new File(Config.regDirectory + Config.zipFileName);
            FileInputStream fis = new FileInputStream(f);
            zipFile = new DefaultStreamedContent(fis, new MimetypesFileTypeMap().getContentType(f), Config.zipFileName);
            
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        
        System.out.println("File downloaded: " + zipFile.getName());
        
    }

    // Getter
    
    public StreamedContent getZipFile() {
        return zipFile;
    }
    
}
