
package control;

import comp.Config;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.event.ActionEvent;
import javax.activation.MimetypesFileTypeMap;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;


/**
 * Update and store the list of files for the admin/index page
 */

@ManagedBean
@ViewScoped
public class AdminController {
    
    private List<HwFile> files;

    public AdminController() {
        getFileList();
    }
    
    public void getFileList()
    {
        File directory = new File(Config.directory);
        files = new ArrayList<HwFile>();
        for( File f : directory.listFiles()){
            
           HwFile hwf = new HwFile();
           
           hwf.setName(f.getName());
           hwf.setSize(f.length());
           hwf.setUploadTime(f.lastModified());
          
           files.add(hwf);
           System.out.println( f.getName() );
        }
    }

    // Getters and setters
    
    public List<HwFile> getFiles() {
        return files;
    }

    public void setFiles(List<HwFile> files) {
        this.files = files;
    }
  
}
