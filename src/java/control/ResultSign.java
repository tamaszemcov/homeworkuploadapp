
package control;

import javax.faces.bean.ManagedBean;

/**
 * Stores the states of the wrong/right graphical indicators of the given results
 */

@ManagedBean (name = "resultSign")
public class ResultSign {
    private String pnSign = "x_mark.png";
    private String pujSign = "x_mark.png";
    private String przSign = "x_mark.png";

    //Getters and setters
    
    public String getPnSign() {
        return pnSign;
    }

    public void setPnSign(String pnSign) {
        this.pnSign = pnSign;
    }

    public String getPujSign() {
        return pujSign;
    }

    public void setPujSign(String pujSign) {
        this.pujSign = pujSign;
    }

    public String getPrzSign() {
        return przSign;
    }

    public void setPrzSign(String przSign) {
        this.przSign = przSign;
    }
            
         
                   
}
