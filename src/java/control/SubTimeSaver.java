
package control;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

/**
 * Stores the starting date and deadline of submission from the admin/index page
 */

@ManagedBean
@ViewScoped
public class SubTimeSaver implements Serializable{
    
    private Date fromDate;
    private Date endDate;

    
    public SubTimeSaver() {
        
        // opening the properties file
        Properties properties = new Properties();
        try {
            properties.load (new FileInputStream ("prop.properties"));
            }catch (IOException e) {
                e.printStackTrace();
            }
        
        // reading and creating the starting and ending date
        int fromYear = Integer.parseInt(properties.getProperty("fromYear", String.valueOf(new Date().getYear())));
        int fromMonth = Integer.parseInt(properties.getProperty("fromMonth", String.valueOf(new Date().getMonth())));
        int fromDay = Integer.parseInt(properties.getProperty("fromDay", String.valueOf(new Date().getDate())));
        
        fromDate = new Date(fromYear, fromMonth, fromDay); 
        
         
        int endYear = Integer.parseInt(properties.getProperty("endYear", String.valueOf(new Date().getYear())));
        int endMonth = Integer.parseInt(properties.getProperty("endMonth", String.valueOf(new Date().getMonth())));
        int endDay = Integer.parseInt(properties.getProperty("endDay", String.valueOf(new Date().getDate())));
        
        endDate = new Date(endYear, endMonth, endDay);
        
    }
    
    /**
    * Stores the starting date
    */
    public void fromDateChange(){
        
        // opening the properties file
        Properties properties = new Properties();
        try {
            properties.load (new FileInputStream ("prop.properties"));
            }catch (IOException e) {
                e.printStackTrace();
            }
        
        // saving the properties
        properties.setProperty("fromYear", String.valueOf(fromDate.getYear()));
        properties.setProperty("fromMonth", String.valueOf(fromDate.getMonth()));
        properties.setProperty("fromDay", String.valueOf(fromDate.getDate()));
        
        // updating the properties file
        try {
            FileOutputStream s = new FileOutputStream ("prop.properties");
            properties.store (s,"Program properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
    
        System.out.println("fromdate: " + fromDate.toString());
    }

    /**
    * Stores the ending date
    */
    public void endDateChange(){
        
        // opening the properties file
        Properties properties = new Properties();
        try {
            properties.load (new FileInputStream ("prop.properties"));
            }catch (IOException e) {
                e.printStackTrace();
            }
        
        // saving the properties
        properties.setProperty("endYear", String.valueOf(endDate.getYear()));
        properties.setProperty("endMonth", String.valueOf(endDate.getMonth()));
        properties.setProperty("endDay", String.valueOf(endDate.getDate()));
        
        // updating the properties file
        try {
            FileOutputStream s = new FileOutputStream ("prop.properties");
            properties.store (s,"Program properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
    
        System.out.println("endDate: " + endDate.toString());
    }

    // Getters and setters
    
    public Date getFromDate() {
        return fromDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
}
