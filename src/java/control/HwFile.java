
package control;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.bean.ManagedBean;

/**
 * File object of the homework list
 */

@ManagedBean
public class HwFile {
    
    private String name;

    private long size;
    private long uploadTime;
    
    // Methods to get String representations of the properties
    
    public String getSizeString()
    {
        return new String(String.valueOf(size)+" byte");
    }
    
    public String getUploadString()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Date date = new Date(uploadTime);
        return new String(dateFormat.format(date));
    }
    
    public String getTaskNumberString()
    {
        if (name!=null){
        String tmp1 = name.substring(0, name.lastIndexOf("_"));
        String tmp2 = tmp1.substring(0, tmp1.lastIndexOf("_"));
        return tmp2.substring(tmp2.lastIndexOf("_")+1);
        } else return null;
    }
    
    // Method to get int representation of the task number
    
    public int getTaskNumberInt()
    {
                        
        if (name!=null){
        String tmp1 = name.substring(0, name.lastIndexOf("_"));
        String tmp2 = tmp1.substring(0, tmp1.lastIndexOf("_"));
        try {
            return Integer.parseInt(tmp2.substring(tmp2.lastIndexOf("_")+1));
        } catch (NumberFormatException e) {
            return 0;
        } 
        } else return 0;
    }
    
    //Getters and setters
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(long uploadTime) {
        this.uploadTime = uploadTime;
    }

}
