/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import comp.Config;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Inserts records into the register
 */

public class SubmissionRegister {
    
    public static void insertRecord(String operation, String name, String neptun, int taskNo, Date subDate, long size, String fileName )
    {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(Config.regDirectory + Config.regFileName,true);
            
            fileWriter.append(operation);
            fileWriter.append(";");
            fileWriter.append(name);
            fileWriter.append(";");
            fileWriter.append(neptun);
            fileWriter.append(";");
            fileWriter.append(String.valueOf(taskNo));
            fileWriter.append(";");
            
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            fileWriter.append(new String(dateFormat.format(subDate)));
            fileWriter.append(";");
            
            fileWriter.append(new String(String.valueOf(size)+" byte"));
            fileWriter.append(";");
            fileWriter.append(fileName);
            fileWriter.append("\n");

            
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally{
             try {
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException e) {
                System.out.println("Error while flushing/closing fileWriter !!!");
                e.printStackTrace();
            }
        }
    }
}
