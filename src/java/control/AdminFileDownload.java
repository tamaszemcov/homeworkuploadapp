/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import comp.Config;
import java.io.File;
import java.io.FileInputStream;
import java.util.Map;
import javax.activation.MimetypesFileTypeMap;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 * For downloading a given file from the list
 */

@ManagedBean
@RequestScoped
public class AdminFileDownload {
    
    private StreamedContent file;

    public AdminFileDownload() {
        
        // getting the filename from the page as a parameter
        Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String name = (String)params.get("name");
        
        // downloading the file
        try {
            
            File f = new File(Config.directory + name);
            FileInputStream fis = new FileInputStream(f);
            file = new DefaultStreamedContent(fis, new MimetypesFileTypeMap().getContentType(f), name);
            
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        
        System.out.println("File downloaded: " + file.getName());
        
    }

    // Getter
    
    public StreamedContent getFile() {
        return file;
    }
    
}
