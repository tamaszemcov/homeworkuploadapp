
package control;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * Manages the log out of the admin/nidex page
 */

@ManagedBean(name="authentication")
@ViewScoped
public class Authentication implements Serializable {

    public void logout() {
        try {
            System.out.println("LOGOUT");
            ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).logout();
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage("logoutmessage", new FacesMessage(FacesMessage.SEVERITY_INFO, "Log out", "Successful"));
        } catch (ServletException ex) {
            Logger.getLogger(Authentication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
