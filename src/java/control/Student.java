
package control;

import javax.faces.bean.ManagedBean;

/**
 * Stores the data of the student
 */

@ManagedBean(name = "student")
public class Student {
    
    private  String name;
    private  String neptun; //neptun code
    private  int taskno;    //number of the given task

    // Getters and Setters   
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNeptun() {
        return neptun;
    }

    public void setNeptun(String neptun) {
        this.neptun = neptun;
    }

    public int getTaskno() {
        return taskno;
    }

    public void setTaskno(int taskno) {
        this.taskno = taskno;
    }

    
}
