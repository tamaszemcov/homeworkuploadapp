package control;


import comp.Config;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.activation.MimetypesFileTypeMap;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 * Controls the file related functionalities of the index page
 */

@ManagedBean(name="fileManagement") 
@SessionScoped ()
@Named
public class FileManagement implements Serializable {

    private String destination = Config.directory;  // the directory where the files are stored
    private String fileName;
    private StreamedContent file;
    private boolean downloadReady;  //indicates that the file is ready for download
    private Student student;
    
    private boolean uploadready = false;    //indicates that the file is ready for upload
    
    
    public FileManagement() {
        this.student = new Student();
        
    }
    
    
    public void upload(FileUploadEvent event) {
        // getting the file from the client
        UploadedFile fu = event.getFile();
        
        // creating the actual date in given format
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
	Date date = new Date();
        
        // uploading the file
        fileName = new String(student.getName()+"_"+student.getNeptun()+"_"+Controller.studentTaskno+"_"+dateFormat.format(date)+".pdf");
        FacesMessage msg = new FacesMessage("Success! ", fileName + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        uploadready = true;
        try {
            copyFile(fileName, fu.getInputstream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        // recording the upload operation in the register
        SubmissionRegister.insertRecord("Upload", student.getName(), student.getNeptun(), Controller.studentTaskno, date, fu.getSize(), fileName);
         
    }

    /**
    * Uploads the file
    */
    public void copyFile(String fileName, InputStream in) {
        try {

            // write the inputStream to a FileOutputStream
            OutputStream out = new FileOutputStream(new File(destination + fileName));

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }

            in.close();
            out.flush();
            out.close();

            System.out.println("File uploaded!");
            System.out.println(uploadready);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public StreamedContent getFile() {
        return file;
    }
    
    /**
    * Prepares the file for download
    */
    public void prepareDownload(){
        System.out.println("Preparing download:...");
        try {
            File f = new File(destination + fileName);
            FileInputStream fis = new FileInputStream(f);
            file = new DefaultStreamedContent(fis, new MimetypesFileTypeMap().getContentType(f), fileName);
            System.out.println("File being downloaded, size: " + f.length());
        } catch (Exception e) {
            downloadReady = false;
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "File unavailable"));
            System.out.println("Exception: File not found");
            return;
        }
        downloadReady = true;
        System.out.println("File downloaded: " + file.getName());
     }
    
    
    /**
    * invalidates the current session
    */
    public void completed(){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        FacesContext context = FacesContext.getCurrentInstance();        
        context.addMessage("sessionEndMessage", new FacesMessage(FacesMessage.SEVERITY_INFO,"Thank you","Session successfully completed"));
       
        if(session != null){
            session.invalidate();
        }
    }
    
    // Getter and setters
    
    public boolean isDownloadReady() {
        return downloadReady;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean isUploadready() {
        return uploadready;
    }

    public void setUploadready(boolean uploadready) {
        this.uploadready = uploadready;
    }
    
    
}
