
package comp;

/**
 * Configuration constants for the system
 */

public interface Config {
    public static final double limit = 0.01;        // allowed difference between the compared results
    public static final String directory = "C:\\tmp\\";     // Homework files directory  
    public static final String regDirectory = "C:\\tmpreg\\";       // directory for register and archive files
    public static final String regFileName = "submissionregister.csv";      // name of the register file
    public static final String zipFileName = "Submissions.zip";     //name of the zip archive file
}
