
package utils;

import comp.Config;
import entity.Task;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import annotations.Compare;

/**
 * Compares tasks and results
 */

public final class Comparison {

    public static boolean compareNumbers(double d1, double d2) {
        if (Math.abs(d2) < Math.abs(d1) * (1.0 + Config.limit) && Math.abs(d2) > Math.abs(d1) * (1.0 - Config.limit)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param task1 Task to be compared.
     * @param task2 Task to be compared.
     * @return true if fields of task1 and task2 marked by
     * @Compare are identical. If none of the fields are marked by
     * @Compare, the method will return true.
     */
    public static boolean compareTasks(Task task1, Task task2) {
        Field[] fs = Task.class.getDeclaredFields();
        double num1 = 0;
        double num2 = 0;
        boolean res = false;
        for (int i = 0; i < fs.length; i++) {
            fs[i].setAccessible(true);
            if (fs[i].isAnnotationPresent(Compare.class)){ 
                try {
                    num1 = fs[i].getDouble(task1);
                    num2 = fs[i].getDouble(task2);
                    res = compareNumbers(num1, num2);
                    if (res == false) {
                        return false;
                    }
                } catch (IllegalArgumentException ex) {
                    Logger.getLogger(Task.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(Task.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return true;
    }

    private Comparison() {
    }
 
}
