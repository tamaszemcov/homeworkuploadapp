
package entity;

import annotations.Compare;
import java.io.Serializable;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity of the results database
 */

@RequestScoped
@Named
@Entity
@Table(name = "TASK")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Task.findAll", query = "SELECT t FROM Task t"),
    @NamedQuery(name = "Task.findById", query = "SELECT t FROM Task t WHERE t.id = :id"),
    @NamedQuery(name = "Task.findByPn", query = "SELECT t FROM Task t WHERE t.pn = :pn"),
    @NamedQuery(name = "Task.findByPuj", query = "SELECT t FROM Task t WHERE t.puj = :puj"),
    @NamedQuery(name = "Task.findByPrz", query = "SELECT t FROM Task t WHERE t.prz = :prz")})
public class Task implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    
    @Compare
    @Basic(optional = false)
    @Column(name = "PN")
    private double pn;
    
    @Compare
    @Basic(optional = false)
    @Column(name = "PUJ")
    private double puj;
    
    @Compare
    @Basic(optional = false)
    @Column(name = "PRZ")
    private double prz;

    public Task() {
    }

    public Task(Integer id) {
        this.id = id;
    }

    public Task(Integer id, double pn, double puj, double prz) {
        this.id = id;
        this.pn = pn;
        this.puj = puj;
        this.prz = prz;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getPn() {
        return pn;
    }

    public void setPn(double pn) {
        this.pn = pn;
    }

    public double getPuj() {
        return puj;
    }

    public void setPuj(double puj) {
        this.puj = puj;
    }

    public double getPrz() {
        return prz;
    }

    public void setPrz(double prz) {
        this.prz = prz;
    }
}
